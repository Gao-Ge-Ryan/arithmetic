
import java.util.Arrays;

/**
 * 归并排序
 * <p>
 * 归并排序的核心思想:如果要排序一个数组，我们先把数组从中间分成前后两部分，
 * 然后对前后两部分分别排序，再将排好序的两部分合并在一起，这样整个数组就都有序了
 * 我们申请一个临时数组 tmp，大小与 A[p...r]相同。我们用两个游标 i 和 j，
 * 分别指向 A[p...q]和 A[q+1...r]的第一个元素。
 * 比较这两个元素 A[i]和 A[j]，如果 A[i]<=A[j]，我们就把 A[i]放入到临时数组 tmp，
 * 并且 i 后移一位，否则将 A[j]放入到数组 tmp，j 后移一位。
 * 继续上述比较过程，直到其中一个子数组中的所有数据都放入临时数组中，
 * 再把另一个数组中的数据依次加入到临时数组的末尾，这个时候，
 * 临时数组中存储的就是两个子数组合并之后的结果了。
 * 最后再把临时数组 tmp 中的数据拷贝到原数组 A[p...r]中。
 * <p>
 * 在合并的过程中，如果 A[p...q]和 A[q+1...r]之间有值相同的元素，
 * 先把 A[p...q]中的元素放入 tmp 数组。这样就保证了值相同的元素，
 * 在合并前后的先后顺序不变。所以，归并排序是一个稳定的排序算法。
 * <p>
 * 不管是最好情况、最坏情况，还是平均情况，时间复杂度都是 O(nlogn)
 * <p>
 * 临时内存空间最大也不会超过 n 个数据的大小，所以空间复杂度是 O(n)。
 * <p>
 * 归并排序不是原地排序算法。
 * 这是因为归并排序的合并函数，在合并两个有序数组为一个有序数组时，需要借助额外的存储空间。
 *
 * @author gaoge
 * @since 2022/11/29 17:07
 */
public class MergeSort {

    /**
     * 归并排序算法
     *
     * @param a 是数组
     * @param n 表示数组大小
     */
    public static void mergeSort(int[] a, int n) {
        mergeSortInternally(a, 0, n - 1);
    }

    /**
     * 递归调用函数
     *
     * @param a 数组
     * @param p 开始索引
     * @param r 结束索引
     */
    private static void mergeSortInternally(int[] a, int p, int r) {
        // 递归终止条件
        if (p >= r) {
            return;
        }
        // 取p到r之间的中间位置q,防止（p+r）的和超过int类型最大值
        int q = p + (r - p) / 2;
        // 分治递归,光看第一层
        mergeSortInternally(a, p, q);
        mergeSortInternally(a, q + 1, r);

        // 将A[p...q]和A[q+1...r]合并为A[p...r]
        merge(a, p, q, r);
    }

    private static void merge(int[] a, int p, int q, int r) {
        int i = p;
        int j = q + 1;
        int k = 0;
        // 申请一个大小跟a[p...r]一样的临时数组
        int[] tmp = new int[r - p + 1];
        while (i <= q && j <= r) {
            if (a[i] <= a[j]) {
                tmp[k++] = a[i++];
            } else {
                tmp[k++] = a[j++];
            }
        }

        // 判断哪个子数组中有剩余的数据
        int start = i;
        int end = q;
        if (j <= r) {
            start = j;
            end = r;
        }

        // 将剩余的数据拷贝到临时数组tmp
        while (start <= end) {
            tmp[k++] = a[start++];
        }

        // 将tmp中的数组拷贝回a[p...r]
        for (i = 0; i <= r - p; ++i) {
            a[p + i] = tmp[i];
        }
    }

    /**
     * 合并(哨兵)
     *
     * @param arr 数组
     * @param p   开始索引
     * @param q   中间索引
     * @param r   结束所有
     */
    private static void mergeBySentry(int[] arr, int p, int q, int r) {
        int[] leftArr = new int[q - p + 2];
        int[] rightArr = new int[r - q + 1];

        for (int i = 0; i <= q - p; i++) {
            leftArr[i] = arr[p + i];
        }
        // 第一个数组添加哨兵（最大值）
        leftArr[q - p + 1] = Integer.MAX_VALUE;

        for (int i = 0; i < r - q; i++) {
            rightArr[i] = arr[q + 1 + i];
        }
        // 第二个数组添加哨兵（最大值）
        rightArr[r - q] = Integer.MAX_VALUE;

        int i = 0;
        int j = 0;
        int k = p;
        while (k <= r) {
            // 当左边数组到达哨兵值时，i不再增加，直到右边数组读取完剩余值，同理右边数组也一样
            if (leftArr[i] <= rightArr[j]) {
                arr[k++] = leftArr[i++];
            } else {
                arr[k++] = rightArr[j++];
            }
        }
    }

    public static void main(String[] args) {
        int[] ints = new int[]{9, 8, 4, 6, 7, 7, 5, 4, 0, 3, 34, 12};
        System.out.println(Arrays.toString(ints));
        mergeSort(ints, ints.length);
        System.out.println(Arrays.toString(ints));
    }
}
